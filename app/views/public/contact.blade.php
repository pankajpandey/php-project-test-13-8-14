@extends('layouts.public')
@section('header')
{{ HTML::style('css/bootstrap.min_1.3.css') }}
@stop

@section('content')

<div class="row">
<div class="col-md-12">
    <div class="block">
        {{ Form::open(array('url' => 'contact-us')) }}
            <div class="form-horizontal" >
                <section>
                    <div class="form-group">
                        {{Form::label('firstname', 'First name *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control', 'required' => '')) }}
                          </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('surname', 'Surname *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('surname', Input::old('surname'), array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('email', 'Email address *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('contact_no', 'Daytime contact number *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('contact_no', Input::old('contact_no'), array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                </section>
                <section>
                    <div class="form-group">
                        {{Form::label('address', 'Address *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('address', Input::old('address'), array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('suburb', 'Suburb *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('suburb', Input::old('suburb'), array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('state', 'State *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::select('state', array('' => '-- Select --', 'ACT' => 'ACT','NSW' => 'NSW','NT' => 'NT','QLD' => 'QLD','SA' => 'SA','TAS' => 'TAS','VIC' => 'VIC','WA' => 'WA'),'',array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('postcode', 'Postcode *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('postcode', Input::old('postcode'), array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                </section>
                <section class="last">
                    <div class="form-group">
                        {{Form::label('enq_type', 'Enquiry type *', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::select('enq_type', array('' => '-- Select --', 'GE' => 'General enquiry','PFE' => 'Product feedback or enquiry','PC' => 'Product complaint'),'',array('class' => 'form-control', 'required' => '')) }}
                        </div>
                    </div>
                    @if ($errors->has('pro_name'))
                    <label class="error-msg">Field can not be empty!</label>
                    @endif
                    <div class="form-group pro-validate">
                        {{Form::label('pro_name', 'Product name', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('pro_name', Input::old('pro_name'), array('class' => 'form-control')) }}
                        </div>
                    </div>
                    @if ($errors->has('pro_size'))
                    <label class="error-msg">Field can not be empty!</label>
                    @endif
                    <div class="form-group pro-validate">
                        {{Form::label('pro_size', 'Product size', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('pro_size', Input::old('pro_size'), array('class' => 'form-control')) }}
                        </div>
                    </div>
                    @if ($errors->has('useby_date'))
                    <label class="error-msg">Field can not be empty!</label>
                    @endif
                    <div class="form-group pro-validate">
                        {{Form::label('useby_date', 'Use-by date', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('useby_date', Input::old('useby_date'), array('class' => 'form-control')) }}
                        </div>
                    </div>
                    @if ($errors->has('batch_code'))
                    <label class="error-msg">Field can not be empty!</label>
                    @endif
                    <div class="form-group pro-validate">
                        {{Form::label('batch_code', 'Batch code', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::text('batch_code', Input::old('batch_code'), array('class' => 'form-control')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('enquiry', 'Enquiry', array('class' => 'col-md-4 control-label'));}}
                        <div class="col-md-5">
                            {{ Form::textarea('enquiry', Input::old('enquiry'), array('class' => 'form-control')) }}
                        </div>
                    </div>
                </section>
                <div class="form-group form-actions">
                    <div class="col-md-4 col-md-offset-5">
                        {{  Form::submit('Submit >', array('name' => 'submit-form', 'class' => 'btn btn-sm btn-primary btn-contact')) }}
                    </div>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
</div>
@stop