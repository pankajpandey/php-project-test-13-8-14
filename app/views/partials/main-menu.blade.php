<?php

// For now build the menu manually
// TODO: Build a class or data store for menu items, possibly use laravel administrator for public facing end
$topMenu = array(
    (object) array('name' => 'How it Works', 'href' => '/how-it-works', 'children' => array(
        (object) array('name' => 'Raising Money', 'href' => '/how-it-works/raising-money'),
        (object) array('name' => 'Investing', 'href' => '/how-it-works/investing')
    )),
    (object) array('name' => 'Members', 'href' => '/members'),
    (object) array('name' => 'Support', 'href' => '/support'),
    (object) array('name' => 'Join', 'href' => '/join')
);
?>

<!-- Main Menu -->
<div id="main-menu" class="topbar-wrapper" data-ng-controller="MainMenuController" data-ng-cloak>
    <div class="row">
        <nav class="top-bar" data-topbar data-ng-hide="hideMenu">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="{{ url() }}">Many Amigos</a></h1>
                </li>
                <li class="toggle-topbar menu-icon">
                    <a href="#"><span>Menu</span></a>
                </li>
            </ul>

            <section class="top-bar-section">
                <ul class="right">
                    @foreach ($topMenu as $menuItem)
                    <?php $hasChildren = property_exists($menuItem, 'children'); ?>
                    <li class="@if ($hasChildren) has-dropdown @endif">
                        <a href="{{ url($menuItem->href) }}">{{ $menuItem->name }}</a>
                        @if ($hasChildren)
                        <ul class="dropdown">
                            @foreach ($menuItem->children as $subMenuItem)
                            <li><a href="{{ url($subMenuItem->href) }}">{{ $subMenuItem->name }}</a></li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </section>
        </nav>
    </div>
</div>