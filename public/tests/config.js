(function(global) {
    global.monkeytestjs = {
        "local": {
            "env": ['http://manyamigos.dev/']
        },
        "testsDir": "mytests",
        "globalTests": [
            "global/not_server_error.js",
            "global/is_html_w3c_valid.js",
            "global/has_utf8_metatag.js"
        ],
        "pages": [
            {
                "url": "/contact-us",
                "tests": [ "page/is_html_w3c_valid.js", "page/has_utf8_metatag.js", "page/not_server_error.js"]
            }
        ],
        "proxyUrl": "core/proxy.php?mode=native&url=<%= url %>",
        "loadSources": true
    };
})(this);